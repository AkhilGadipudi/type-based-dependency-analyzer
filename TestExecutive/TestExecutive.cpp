/////////////////////////////////////////////////////////////////////
// TestExecutive.cpp -  Demonstrates use of CodePublisher		   //
// Author : Akhil Gadipudi    SU email : agadipud@syr.edu          //
// CSE687 - OOD, Spring 2017									   //
/////////////////////////////////////////////////////////////////////

#include <fstream>
#include"../CodePublisher/CodePublisher.h"
int main(int argc, char* argv[])
{
	CodePublisher cp;
	std::cout << "\n\nWritten in C++ using Visual Studio 2015 - Req #1\n";
	std::cout << "Used only C++ std lib for I/O and new-delete for heap-memory - Req #2\n";
	if (std::string(argv[1]) == "..\\")
		std::cout << "Running CodePublisher on itself to demonstrate - Req #9\n";
	cp.processCommandLineArgs(argc, argv);
	cp.createIndex();
	cp.publish();
	cp.openHtml();
	std::cout << "Provided CodePublisher package that creates html for the files - Req #3\n";
	std::cout << "Provided funtionality to collapse and expand code blocks - Req #4\n";
	std::cout << "CSS and JavaScript files provided in ../PublishedFiles/styles and ../PublishedFile/scripts respectively - Req #5\n";
	std::cout << "Embedded CSS and JS files in head section of all html files - Req #6\n";
	std::cout << "Dependencies of each file obatined using proj2 DepAnalyzer, embedded links to dependencies in every htm file - Req #7\n";
	std::cout << "This TestExecutive demonstrates all requirments - Req #10\n";
	return 0;
}

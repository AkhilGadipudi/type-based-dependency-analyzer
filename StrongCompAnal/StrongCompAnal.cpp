/////////////////////////////////////////////////////////////////////
// StrongCompAnal.cpp -  Identify Strong Componenets sets 		   //
// Author : Akhil Gadipudi    SU email : agadipud@syr.edu          //
// CSE687 - OOD, Spring 2017									   //
/////////////////////////////////////////////////////////////////////
#include<iostream>
#include<fstream>
#include<string>
#include <algorithm>
#include"StrongCompAnal.h"
#include"../Graph/Graph.h"
#include"../NoSqlDb/XmlDocument/XmlDocument/XmlDocument.h"
#include"../NoSqlDb/XmlDocument/XmlElement/XmlElement.h"
using namespace std;

//populates strongComps using Graph and depTable
void StrongCompAnal::doStrongCompAnal()
{
	cout << "\n\n\n Starting Strong Component Analysis:\n";
	Graph g(int(files.size()));
	for (size_t i = 0; i < files.size(); i++)
	{
		string file = files[i];
		vector<string> deps = depTable[file];
		vector<string> depIndex;
		for (auto dep : deps)
		{
			int j = (find(files.begin(), files.end(), dep) - files.begin());
			g.addEdge(i, j);
		}
	}
	g.SCC();
	vector<vector<int>> graphRes = g.getResults();
	for (size_t scSet = 0; scSet < graphRes.size(); scSet++)
	{
		vector<int> elms = graphRes[scSet];
		vector<string> resElms;
		for (auto elm : elms)
			resElms.push_back(files[elm]);
		strongComps.push_back(resElms);
	}
}

// Prints strongComps contents
void StrongCompAnal::show()
{
	cout << " ============   Strong Components   =============      - Req #6\n";
	int i = 1;
	for (auto set : strongComps)
	{
		cout << "\n---- Set #" << i << " ----" << endl;
		for (auto item : set)
			cout << "   " << item << endl;
		i++;
	}
}

// Saves strongComps contents in Xml format and writes to xml file
void StrongCompAnal::toXml(string filename)
{
	size_t nameSize = filename.find_last_of(".") - filename.find_last_of("/\\");
	filename = "../StrongComponents-" + filename.substr(filename.find_last_of("/\\") + 1, nameSize) + ".xml";
	using namespace XmlProcessing;
	using SPtr = std::shared_ptr<AbstractXmlElement>;
	std::string xml;
	XmlDocument doc;
	SPtr pRoot = makeTaggedElement("StrongComponents");
	doc.docElement() = pRoot;
	int i = 1;
	for (auto set : strongComps)
	{
		SPtr setTag = makeTaggedElement("Set");
		setTag->addAttrib("Number", to_string(i));
		for (auto item : set)
			setTag->addChild(makeTextElement(item));
		pRoot->addChild(setTag);
		i++;
	}
	xml = doc.toString();
	std::ofstream fs(filename);
	fs << xml;
	fs.close();
	std::cout << "\n\nStrong Components Xml saved to file: " << filename <<"   - Req #7"<< std::endl;
	xml.erase(std::remove(xml.begin(), xml.end(), '\n'), xml.end());
	xml.erase(std::remove(xml.begin(), xml.end(), ' '), xml.end());
	cout << " Strong Components Xml Contents :  \n" <<
		" (Removed newlines and spaces to reduce output size, check xml file for proper xml structure)\n\n";
	cout << xml << endl;
}

#ifdef TEST_StrongComp
int main()
{
	TypeAnal ta;
	ta.doTypeAnal();
	TypeTable tt = ta.getTypeTable();
	DepAnalyzer da("../Tester", tt);
	da.doDepAnal();
	StrongCompAnal sc(da.getDepTable());
	sc.doStrongCompAnal();
	sc.show();
	sc.toXml("Tester-StrongComps");
}
#endif
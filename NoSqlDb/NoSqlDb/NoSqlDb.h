#pragma once
/////////////////////////////////////////////////////////////////////
// NoSqlDb.h - key/value pair in-memory database                   //
// Author : Akhil Gadipudi    SU email : agadipud@syr.edu          //
// Source : Dr. Jim Fawcett, CSE687 - OOD, Spring 2017             //
/////////////////////////////////////////////////////////////////////
/*
* Package information:
* --------------------
* This package provides various funtions to create and maintain a in-memory
* persistent Key-Value database.
*
* Public Interface :
* ------------------
* Element<type> elm()    - New element - replave type with required data type
* elm.name - add name to elm - similarly for description, category, data, timeData
* elm.addChild(key) - Add child to elm
* elm.removeChild(key) - Removes child from elm
* elm.show() - displays elm contents
*
* NoSqlDb<type> db() - new db with data of type "type"
* NoSqlDb<type> db(filepath) - create db and retore content from file
* db.addElem(key, elm) - all element
* db.removeElem(key) - remove element
* db.updateElem(key, elm2) - replace elm with elm2
* db.getKeys - return all keys
* db.saveDB(filepath) - save db to xml file
* db.restoreDB(filepath) - retore db content from Xml file
*
*
* Required files:
* ---------------
* NoSqlDb.h, NoSqlDb.cpp
* StrHelper.h, StrHelper.cpp
* XmlDocument.h, XmlDocument.cpp
* XmlElement.h, XmlElement.cpp
* CppProperties.h, CppProperties.cpp
* Convert.h, Convert.cpp
*
*
* Build Process:
* --------------
*  devenv NoSqlDb.sln /debug rebuild
*
* Maintenance History:
* -------------------
* ver 1.0 : 2/7/2017
*    - First release
*/

#include <unordered_map>
#include <string>
#include <sstream>
#include <vector>
#include <iomanip>
#include <iostream>
#include <chrono>
#include <thread>
#include <functional>
#include <fstream>
#include <regex>
//#include "../CppProperties/CppProperties.h"
#include "../XmlDocument/XmlDocument/XmlDocument.h"
#include "../XmlDocument/XmlElement/XmlElement.h"
#include "../Convert/Convert.h"
#include "../StrHelper/StrHelper.h"

//---< Funtion to get current time in a desired format >----//
static std::string timeString()
{
	std::time_t t = time(NULL);
	char str[26];
	struct tm buf;
	localtime_s(&buf, &t);
	strftime(str, 26, "%Y-%m-%d %H:%M:%S", &buf);
	return trim(std::string(str));
}

/////////////////////////////////////////////////////////////////////
// Element class represents a data record in our NoSql database

template<typename Data>
class Element
{
public:
  using Name = std::string;
  using Category = std::string;
  using TimeDate = std::string;
  using Description = std::string;
  using Key = std::string;

 /* Property<Name> name;           
  Property<Category> category;    
  Property<Description> description; 
  Property<TimeDate> timeDate;   
  Property<Data> data;       */   
  Name name;
  Category category;
  Description description;
  TimeDate timeDate;
  Data data;

  std::string show();
  void addChild(Key key);
  void removeChild(Key key);
  std::vector<Key> getChildren();
  void clear();

private:
	std::vector<Key> children;
};

//----< Adds child or relationship to exixting element in the DB >---//
template<typename Data>
void Element<Data>::addChild(Key key)
{
	std::vector<Key>::iterator it = find(children.begin(), children.end(), key);
	if(it == children.end())
		children.push_back(key);
}

//----< Removes child or relationship to exixting element in the DB >---//
template<typename Data>
void Element<Data>::removeChild(Key key)
{
	std::vector<Key>::iterator it = find(children.begin(), children.end(), key);
	if(it != children.end()) children.erase(it);
}

//----< Returns vector of children for an exixting element in the DB >---//
template<typename Data>
std::vector<std::string> Element<Data>::getChildren()
{
	return children;
}

//----< clear an object of Eleement to reuse same object to handle multiple items of the DB >---//
template<typename Data>
void Element<Data>::clear()
{
	name = "";
	category = "";
	description = "";
	timeDate = "";
	children.clear();
	data = Data();
}

//---< Display contents of an element of the DB >---//
template<typename Data>
std::string Element<Data>::show()
{
  std::ostringstream out;
  out.setf(std::ios::adjustfield, std::ios::left);
  out << "\n    " << std::setw(12) << "name"     << " : " << name;
  out << "\n    " << std::setw(12) << "category" << " : " << category;
  //out << "\n    " << std::setw(12) << "description" << " : " << description;
  out << "\n    " << std::setw(12) << "timeDate" << " : " << timeDate;
  out << "\n    " << std::setw(12) << "children" << " : ";
  for (auto& child : children)
  {
	  out << child << ", ";
  }
  out << "\n    " << std::setw(12) << "data"     << " : " << data;
  out << "\n----------------------------------------------------------------";
  return out.str();
}

/////////////////////////////////////////////////////////////////
// NoSqlDb class; object of this class represents a DB
template<typename Data>
class NoSqlDb
{
public:
  using Key = std::string;
  using Keys = std::vector<Key>;
  NoSqlDb() {}
  NoSqlDb(std::string fileName) { XMLfileName = fileName;  restoreDB(fileName); }
  ~NoSqlDb() { if(XMLfileName != "") saveDB(XMLfileName);  }

  Keys getKeys();
  bool addElem(Key key, Element<Data> elem);
  bool updateElem(Key key, Element<Data> elem);
  bool deleteElem(Key key);
  Element<Data> value(Key key);
  size_t count();
  void restoreDB(std::string fileName);
  void saveDB(std::string fileName);
  void toggle_timed_saver();
  void timed_saver();
 /* Property<int> writes_to_save = 2;
  Property<int> time_to_save;
  Property<int> write_counter = 0;*/

  int writes_to_save = 2;
  int time_to_save;
  int write_counter = 0;

private:
  using Item = std::pair<Key, Element<Data>>;
  std::unordered_map<Key,Element<Data>> store;
  std::string XMLfileName;
  bool timed_saver_toggle = false;

};

//---< return keys of all items in DB >---//
template<typename Data>
typename NoSqlDb<Data>::Keys NoSqlDb<Data>::getKeys()
{
  Keys keys;
  for (Item item : store)
  {
    keys.push_back(item.first);
  }
  return keys;
}

//---< Add a key-value pair to the DB, doesnt allow duplicate keys >---//
template<typename Data>
bool NoSqlDb<Data>::addElem(Key key, Element<Data> elem)
{
  if(store.find(key) != store.end())
    return false;
  store[key] = elem;
  return true;
}

//----< Update the value of specified key in the DB >---//
template<typename Data>
bool NoSqlDb<Data>::updateElem(Key key, Element<Data> elem)
{
	if (store.find(key) == store.end())
		return false;
	store[key] = elem;
	return true;
}

//----< Delete an element from the DB >---//
template<typename Data>
bool NoSqlDb<Data>::deleteElem(Key key)
{
	if (store.find(key) == store.end())
		return false;
	store.erase(key);
	return true;
}

//---< Returns value of an item with specified key >---//
template<typename Data>
Element<Data> NoSqlDb<Data>::value(Key key)
{
  if (store.find(key) != store.end())
    return store[key];
  return Element<Data>();
}

//----< To find the number of elements in the DB >---//
template<typename Data>
size_t NoSqlDb<Data>::count()
{
  return store.size();
}

//----< Save the elements of the DB to an xml file with specified name >---//
template<typename Data>
void NoSqlDb<Data>::saveDB(std::string fileName)
{
	using namespace XmlProcessing;
	using SPtr = std::shared_ptr<AbstractXmlElement>;
	std::string xml;
	XmlDocument doc;
	SPtr pRoot = makeTaggedElement("NoSqlDb");
	doc.docElement() = pRoot;
	Keys keys = getKeys();
	for (Key key : keys)
	{
		SPtr pairTag = makeTaggedElement("Pair");
		pRoot->addChild(pairTag);
		SPtr keyTag = makeTaggedElement("Key");
		pairTag->addChild(keyTag);
		keyTag->addChild(makeTextElement(key));
		SPtr valueTag = makeTaggedElement("Value");
		pairTag->addChild(valueTag);
		SPtr nameTag = makeTaggedElement("Name");
		nameTag->addChild(makeTextElement(store[key].name));
		valueTag->addChild(nameTag);
		SPtr catTag = makeTaggedElement("Category");
		catTag->addChild(makeTextElement(store[key].category));
		valueTag->addChild(catTag);
		SPtr descTag = makeTaggedElement("Description");
		descTag->addChild(makeTextElement(store[key].description));
		valueTag->addChild(descTag);
		SPtr tdTag = makeTaggedElement("TimeDate");
		tdTag->addChild(makeTextElement(store[key].timeDate));
		valueTag->addChild(tdTag);
		SPtr childrenTag = makeTaggedElement("Children");
		valueTag->addChild(childrenTag);
		Keys childKeys = store[key].getChildren();
		childrenTag->addAttrib("count", Convert<int>::toString(childKeys.size()));
		for (auto& child : childKeys)
			childrenTag->addChild(makeTextElement(child));
		SPtr dataTag = makeTaggedElement("Data");
		dataTag->addChild(makeTextElement(Convert<Data>::toString(store[key].data)));
		valueTag->addChild(dataTag);
	}
	xml = doc.toString();
	std::ofstream file(fileName);
	file << xml;
	file.close();
	std::cout << "DB saved to file: " << fileName << std::endl;
}

//----< Restore DB from an xml file >---//
template<typename Data>
void NoSqlDb<Data>::restoreDB(std::string fileName)
{
	using namespace XmlProcessing;
	using SPtr = std::shared_ptr<AbstractXmlElement>;
	using SPtrs = std::vector<SPtr>;
	XmlDocument doc(fileName, XmlDocument::file);
	std::vector<SPtr> pairs = doc.descendents("Pair").select();
	int num = pairs.size();
	for (int i=0; i<num;i++)
	{
		Element<Data> item;
		Key key;
		SPtrs readKey = doc.descendents("Key").select();
		key = trim(readKey[i]->children()[0]->value());

		SPtrs names = doc.descendents("Name").select();
		item.name = trim(names[i]->children()[0]->value());

		SPtrs categories = doc.descendents("Category").select();
		item.category = trim(categories[i]->children()[0]->value());

		SPtrs descriptions = doc.descendents("Description").select();
		item.description = trim(descriptions[i]->children()[0]->value());

		SPtrs timeDates = doc.descendents("TimeDate").select();
		item.timeDate = trim(timeDates[i]->children()[0]->value());

		SPtrs datas = doc.descendents("Data").select();
		item.data = Convert<Data>::fromString(trim(datas[i]->children()[0]->value()));

		SPtrs childitems = doc.descendents("Children").select();
		if (childitems[i]->children().size() != 0)
		{
			std::string childrenString = childitems[i]->children()[0]->value();
			std::istringstream childrenStream(childrenString);
			std::vector<std::string> childrenFound;
			std::string childFound;
			while (std::getline(childrenStream, childFound, '\n'))
			{
				childrenFound.push_back(childFound);
			}
			for (size_t j = 0; j < (childrenFound.size() - 1); j++)
			{
				item.addChild(trim(childrenFound[j]));
			}
		}
		addElem(key, item);
		item.clear();
	}
}

//---< Toggles the timed saver ON and OFF, set to OFF by default >---//
template<typename Data>
void NoSqlDb<Data>::toggle_timed_saver()
{
	(timed_saver_toggle == true) ? timed_saver_toggle = false : timed_saver_toggle = true;	
}

//---< Starts timed saver, uses threads and calls saveDB method at specified time intervals >---//
template<typename Data>
void NoSqlDb<Data>::timed_saver()
{
	std::function<void(void)> func = NoSqlDb<Data>::saveDB(XMLfileName);
	std::thread([func, interval]() {
		while (timed_saver_toggle)
		{
			func();
			std::this_thread::sleep_for(std::chrono::minutes(time_to_save));
		}
	}).detach();
}

#include <iostream>
#include <string>
#include<vector>
#include "../NoSqlDb/NoSqlDb.h"
#include "../Queries/Queries.h"

using namespace std;

void print_res(vector<string> res)
{
	for (string r : res)
	{
		cout << r << endl;
	}
	cout << endl;
}
void disp_intdb(NoSqlDb<int> db)
{
	for (string key : db.getKeys())
	{
		cout << "Key         :  " << key;
		cout << db.value(key).show();
		cout << endl;
	}
}

void demo_intDb()
{
	cout << "Demo of Int DB\n";
	NoSqlDb<int> db = NoSqlDb<int>("../IntDb.xml");
	cout << "Loading DB from \"IntDb.Xml\"					-----  Req #5\n";
	cout << "Int DB contents:\n";
	disp_intdb(db);
	cout << "\n\nAugmenting Int DB from \"MoreIntDb.xml\"	----  Req #5\n";
	db.restoreDB("../MoreIntDb.xml");
	cout << "New Int DB contents:\n";
	disp_intdb(db);
	cout << "Adding new element of key \"key-105\"			---  Req #3\n";
	Element<int> item;
	item.name = "new item";
	item.category = "added items";
	item.description = "Added to DB in demo";
	item.data = 555;
	item.timeDate = timeString();
	db.addElem("key-105", item);
	cout << "Deleting element of key \"key-103\"			----  Req #3\n";
	db.deleteElem("key-103");
	cout << "Editing existing DB element					----  Req #4  \n";
	item = db.value("key-101");
	item.addChild("key-102");
	item.category = "parent";
	item.name = "item1.1";
	db.updateElem("key-101", item);
	cout << "Added relationship, changed category and name of item with key \"key-101\" \n\n";
	cout << "Current status of DB after insertion, deletion and editing\n";
	disp_intdb(db);
	cout << "Performing query on Int DB to get keys of elements of category \"new nums\"\n";
	Query<int> q;
	Keys r1 = q.KeysByCategory(db, "new nums");
	cout << "Result of the query: \n";
	print_res(r1);
}

void demo_StrDb()
{
	cout << "===================================================================================\n";
	cout << "DB element contains all required metadata		----  Req #2\n";
	cout << "Demoing DB with another data type(string) using template			---- Req #2\n\n";
	cout << "Loading from \"StrDb.xml\"\n";
	cout << "\"StrDb.xml\" contains details on packages and their dependancies in this project	---  Req #10\n\n";
	NoSqlDb<string> db = NoSqlDb<string>("../StrDB.xml");
	cout << "time_to_save to save db at time interval is set to " << db.time_to_save << " minutes\n";
	cout << "Timed save is turned off by default		----   Req #6\n";
	cout << "Contents of this DB\n\n";
	for (string key : db.getKeys())
	{
		cout << "Key         :  " << key;
		cout << db.value(key).show();
		cout << endl;
	}
	cout << "=======================================================================================\n";
	cout << "Queries Demonstration				-----    Req #7\n";
	cout << "Using regex to compare strings in queries		----   Req #12\n";
	Query<string> q;
	using Res = vector<string>;
	cout << "Query : Value with key \"Queries.h\"\nResult :\n";
	cout << q.getValue(db, "Queries.h").show();
	cout << "\nQuery : Children of \"NoSqlDb.h\"\nResult (R1):\n";
	Res R1 = q.getChildren(db, "NoSqlDb.h");
	print_res(R1);
	cout << "Query : Keys containing the string\"Xml\"\nResult (R2):\n";
	Res R2 = q.getKeys(db, "Xml");
	print_res(R2);
	cout << "Query : Keys of elements that contain \"cpp\" in their name\nResult (R3):\n";
	Res R3 = q.KeysByName(db, "cpp");
	print_res(R3);
	cout << "Query : Keys of elements that contain \"Header file\" in their category\nResult (R4):\n";
	Res R4 = q.KeysByCategory(db, "Header file");
	print_res(R4);
	cout << "Query : Keys of elements that contain the string \"Documenet\" in their data\nResult (R5):\n";
	Res R5 = q.KeysByData(db, "Document");
	print_res(R5);
	cout << "Query : Keys of elements written between 2017-02-07 07:00:00 and " << timeString() << "\nResult (R6):\n";
	Res R6 = q.KeysByTimeIntv(db, "2017-02-07 07:00:00");
	print_res(R6);
	cout << "\n===   Compound Queries   =====\n";
	cout << "Query : Get Header files that contain Xml in name i.e filter R4 using the string Xml	---  Req #8\nResult:\n";
	Res R7 = q.getKeys(db, "Xml", R4);
	print_res(R7);
	cout << "Query : Uninon of the results R3 and R4		---   Req #9\nResult:\n";
	Res R8 = q.Union(R3, R4);
	print_res(R8);
}
int main()
{
	cout << "Implemented in c++ using standard c++ libraries and VS2015    ----   Req #1\n\n";
	demo_intDb();
	cout << "Note that xml file will be updated when application closes\n"
		<< "2 or more execution of the aplication will show different values in the data base\n\n";
	demo_StrDb();
	cout << "Demonstrated Req #2 - Req #9				-----   Req #11\n\n";
	getchar();
}
#ifndef GRAPH_H_
#define GRAPH_H_
/////////////////////////////////////////////////////////////////////
// Graph.h -  Graphs and operate on them						   //
// Author : Akhil Gadipudi    SU email : agadipud@syr.edu          //
// CSE687 - OOD, Spring 2017									  //
/////////////////////////////////////////////////////////////////////
/*
	Operation :
	Creates graphs and performs operations to find out strong component sets
	in the graph using Tarjan's algorithm

	Public Interface :
	Graph g(numOfVertex)  // create graph object
	g.addEdge(i,j)			// create edge between vertex i,j - i and j are less than numOfVertex
	g.SCC()			// find string component sets
	g.getResults()	// return vector<vector<int>> - multiple SC sets with 1 or more 
						vetices in each set
	Required Files:
	Graph.h, Gprah.cpp

	Build Command:
	devenv Gprah.sln /rebuild debug

	Maintenance History:
	ver 1.0 - March 7, 2017 - Initial release
*/
#include<iostream>
#include <list>
#include <stack>
#include <vector>

// A class that represents a directed graph
	class Graph
	{
	private:
		int V;
		std::list<int> *adj;
		void SCCUtil(int u, int disc[], int low[], std::stack<int> *st, bool stackMember[]);
		int min(int x, int y);
		std::vector<std::vector<int>> strComps;
	public:
		Graph(int V)
		{
			this->V = V;
			adj = new std::list<int>[V];
		}
		
		void addEdge(int v, int w);
		void SCC();
		std::vector<std::vector<int>> getResults();
	};
#endif // !GRAPH_H_
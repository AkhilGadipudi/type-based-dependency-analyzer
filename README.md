# Type Based Dependency Analyzer

*	Identified various type definitions, global functions, and data in every file to create type table
*	Figured out dependencies of files by analyzing the types used in the file and the data from the type table
*	Identified Strong Component sets using the dependency information.
*	Designed it to work on various programming languages, tested on C#, C++.
---
This was developed as part of CSE687 - Object Oriented Design, so some part of code is provided as a starter code by our professor [Dr. Jim Fawcett](http://ecs.syr.edu/faculty/fawcett/handouts/webpages/fawcettHome.htm)

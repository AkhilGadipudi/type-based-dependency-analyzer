/////////////////////////////////////////////////////////////////////
// TypeAnal.cpp -  Create TypeTable from AST					   //
// Author : Akhil Gadipudi    SU email : agadipud@syr.edu          //
// CSE687 - OOD, Spring 2017									  //
/////////////////////////////////////////////////////////////////////
#include<string>
#include<sstream>
#include "TypeAnal.h"
//#include "../FileSystem/FileSystem.h"
//#include "../TypeTable/TypeTable.h"
using namespace CodeAnalysis;

// To filter only types required for DepAnalysis to add to type table
bool doDisplay(ASTNode* pNode)
{
	static std::string toDisplay[] = {
		"lambda", "class", "struct", "enum", "alias", "typedef","namespace"
	};
	for (std::string type : toDisplay)
	{
		if (pNode->type_ == type)
			return true;
	}
	return false;
}

//checks if two give ASTNodes are parent and child
bool parentAndChild(ASTNode* prt, ASTNode* cld)
{
	for (auto pChild : prt->children_)
		if (pChild == cld)
			return true;
	return false;
}
// recursive function to travese AST and add types to type table
void TypeAnal::DFS(ASTNode* root, ASTNode* pNode)
{
	static std::string path = "";
	static std::string currNamespace;
	if (parentAndChild(root,pNode)) // used to check if node is child of root which is global namespace
	{
		currNamespace = "Global Namespace";
	}
	if (pNode->type_ == "namespace")//capture namespace to assign namespace to children
		currNamespace = pNode->name_;
	
	if (pNode->path_ != path)
	{
		path = pNode->path_;
	}

	//identify global funtions
	if (pNode->type_ == "function" && pNode->parentType_ == "namespace" /*&& currNamespace == "Global Namespace"*/)
	{
		TypeInfo ti;
		size_t pos = path.find_last_of("/\\");
		ti.filename = path.substr(pos + 1);
		ti.path = path;
		ti.type = pNode->type_;
		ti.typenamespace = currNamespace;
		if(pNode->name_ != "main")
			tt.addType(pNode->name_, ti);
	}
	if (doDisplay(pNode))
	{
		TypeInfo ti;
		size_t pos = path.find_last_of("/\\");
		ti.filename = path.substr(pos + 1);
		ti.path = path;
		ti.type = pNode->type_;
		ti.typenamespace = currNamespace;
		tt.addType(pNode->name_, ti);
	}
	for (auto pChild : pNode->children_)
		DFS(root,pChild);
}

// Gets AST and starts recusive funtion to create type table
void TypeAnal::doTypeAnal()
{
	std::cout << "\n Starting type analysis using AST";
	ASTNode* pRoot = ASTref_.root();
	DFS(pRoot,pRoot);
	std::cout << "\n Type Analysis Done, TypeTable created";
}

// return the created type table
TypeTable TypeAnal::getTypeTable()
{
	return tt;
}
#ifdef TEST_TYPEANAL

int main()
{
	TypeAnal ta;
	ta.doTypeAnal();
	TypeTable tt = ta.getTypeTable();
	tt.show();
}
#endif
